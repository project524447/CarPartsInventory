<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /**
     * Display register page.
     * 
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('auth.register');
    }

    /**
     * Handle account registration request
     * 
     * @param RegisterRequest $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function register(RegisterRequest $request) 
    {
        $data = collect( $request->validated() );
        $slug = Str::slug( $request->firstname.' '.$request->lastname, '-' );

        $checkSlug = User::whereSlug( $slug )->exists();

        if ( $checkSlug ) {

            $numericalPrefix = 1;

            while (1) {
                
                $newSlug = $slug."-".$numericalPrefix++;
                $newSlug = Str::slug( $newSlug );

                $checkSlug = User::whereSlug( $newSlug )->exists();

                if ( !$checkSlug) {

                    $slug = $newSlug;

                    break;
                
                }
            }

        }

        $data->offsetSet('slug', $slug);
        $data->offsetSet('api_token', Str::random(80));

        $user = User::create( $data->toArray() );

        auth()->login($user);

        return redirect('/')->with('success', "Account successfully registered.");
    }
}
