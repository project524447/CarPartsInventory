<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

class ResourceController extends Controller
{
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'file' => ['required', 'max:'.$request->max, 'mimes:'.$request->mimes],
        ]);

        Image::configure(['driver' => 'imagick']);
        
        //DEFINE $newDirectoryIndex VARIABLE
        $newDirectoryIndex = 1;

        //IF THE Uploads FOLDER DOES NOT EXIST ON THE DISK, WE WILL CREATE IT
        if ( !is_dir( public_path('uploads'))): mkdir('uploads/', 0755); endif;

        //IF THE Resources FOLDER DOES NOT EXIST IN THE Uploads FOLDER, WE WILL CREATE IT
        if ( !is_dir( public_path('uploads/resources'))): mkdir('uploads/resources/', 0755); endif;

        //THE PROCESS OF CREATING A $folder NAME
        $folders = File::directories( public_path('uploads/resources'));

        usort( $folders, function( $a, $b ) {
            return (int) basename( $a ) - (int) basename( $b );
        });
    
        if ( count( $folders ) > 0 ): $newDirectoryIndex = basename( end( $folders )) + 1; endif;

        //CREATE $folder WITH NAME $newDirectoryIndex
        $folder = 'uploads/resources/'.$newDirectoryIndex;

        mkdir( $folder, 0755 );

        //DEFINE $file VARIABLE
        $file = $request->file('file');

        //DEFINE $name AND $original VARIABLES
        $name = pathinfo( $file->getClientOriginalName(), PATHINFO_FILENAME );
        
        $original = 'original.'.$file->getClientOriginalExtension();

        //SAVE THE $file TO THE $folder
        if ( in_array( $file->getClientOriginalExtension(), [ 'jpg', 'jpeg', 'png', 'gif' ] )):

            Image::make( $file->getRealPath() )->save( public_path( '/'.$folder ).'/'.$original );

            //DEFINE $file SIZE VARIATIONS
            $sizes = config('image.dimensions');

            foreach ( $sizes as $size ):

                Image::make( $folder.'/'.$original )->fit( $size[1], $size[2] )->save( public_path( '/'.$folder ).'/'.$size[0].'.'.$file->getClientOriginalExtension() );

            endforeach;

        else:

            $file->move( public_path( '/'.$folder ), $original );

        endif;

        //SAVE THE $file TO THE DATABASE
        $resource = Resource::create([
            'user_id'   => auth()->user()->id,
            'title'     => $name,
            'slug'      => Str::slug( $name, '-' ),
            'folder'    => '/'.$folder,
            'filetype'  => $file->getClientOriginalExtension(),
            'size'      => $file->getSize(),
        ]);

        return response()->json([
            'item' => $resource,
        ], 201);
    }

    public function destroy( $id )
    {
        $resource = Resource::findOrFail( $id );
        $resource->delete();

        File::deleteDirectory( public_path( $resource->folder ) );

        return response()->json([
            'status'    => true,
            'message'   => 'Zmazanie prebehlo úspešne.',
        ], 204);
    }
}
