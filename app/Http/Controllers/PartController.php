<?php

namespace App\Http\Controllers;

use App\Models\Part;
use App\Models\Car;
use Illuminate\Http\Request;

class PartController extends Controller
{
    /**
     * Display a listing of the parts.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$parts = Part::all();
        $parts = Part::with('car')->get();
        return view('parts.index', compact('parts'));
    }

    /**
     * Show the form for creating a new part.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cars = Car::all(); // Získanie zoznamu áut pre výber pri vytváraní dielu
        return view('parts.create', compact('cars'));
    }

    /**
     * Store a newly created part in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'serialnumber' => 'required',
            'car_id' => 'required|exists:cars,id', // Skontroluje, či zadané ID auta existuje
        ]);

        Part::create($validatedData);

        return redirect()->route('parts.index');
    }

    /**
     * Display the specified part.
     *
     * @param  \App\Models\Part  $part
     * @return \Illuminate\Http\Response
     */
    public function show(Part $part)
    {
        return view('parts.show', compact('part'));
    }

    /**
     * Show the form for editing the specified part.
     *
     * @param  \App\Models\Part  $part
     * @return \Illuminate\Http\Response
     */
    public function edit(Part $part)
    {
        $cars = Car::all();
        return view('parts.edit', compact('part', 'cars'));
    }

    /**
     * Update the specified part in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Part  $part
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Part $part)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'serialnumber' => 'required',
            'car_id' => 'required|exists:cars,id',
        ]);

        $part->update($validatedData);

        return redirect()->route('parts.index');
    }

    /**
     * Remove the specified part from storage.
     *
     * @param  \App\Models\Part  $part
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $part = Part::findOrFail($id);
        $part->delete();
        return response()->json(['message' => 'Part deleted successfully.']);
    }    
}
