import _ from 'lodash'
import { createApp } from 'vue'
import axios from 'axios'
import mixins from './vue/mixins'
const csrfToken = document.querySelector('meta[name="csrf-token"]');
const authorizationMeta = document.querySelector('meta[name="authorization"]');

window._ = _;
window.axios = axios;

/*
window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
    'Authorization': document.querySelector('meta[name="authorization"]').getAttribute('content'),
};*/

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
if (csrfToken) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = csrfToken.getAttribute('content');
}
if (authorizationMeta) {
    window.axios.defaults.headers.common['Authorization'] = authorizationMeta.getAttribute('content');
}

// Importujem komponenty CarsIndex a PartsIndex
import CarsIndex from './vue/components/CarsIndex.vue';
import PartsIndex from './vue/components/PartsIndex.vue';  // Tady je nový import

// Vytvorenie Vue aplikácie
const app = createApp({});

// Registrácia komponentov
//app.component('example-component', ExampleComponent);
app.component('cars-index', CarsIndex);
app.component('parts-index', PartsIndex); 


// Pridanie mixinu
app.mixin(mixins);

// Mount aplikácie Vue
app.mount('#app');
