{{-- resources/views/welcome.blade.php --}}

@extends('layouts.app')

@section('title', 'Car Parts Inventory')

@section('content')
<div class="container h-100">
    <div class="row justify-content-center align-items-center" style="min-height: 100vh;">
        <div class="col-md-8 text-center">
            <h1 class="mb-5">Car Parts Inventory</h1>
            <a href="{{ route('cars.index') }}" class="btn btn-primary me-2">View Cars</a>
            <a href="{{ route('parts.index') }}" class="btn btn-secondary">View Parts</a>
        </div>
    </div>
</div>
@endsection

