{{-- resources/views/parts/create.blade.php --}}

@extends('layouts.app')

@section('title', 'Add New Part')

@section('content')
<div class="container mt-4">
    <h1>Add New Part</h1>
    <form method="POST" action="{{ route('parts.store') }}">
        @csrf
        <div class="mb-3">
            <label for="name" class="form-label">Part Name</label>
            <input type="text" class="form-control" id="name" name="name" required>
        </div>
        <div class="mb-3">
            <label for="serialnumber" class="form-label">Serial Number</label>
            <input type="text" class="form-control" id="serialnumber" name="serialnumber" required>
        </div>
        <div class="mb-3">
            <label for="car_id" class="form-label">Car</label>
            <select class="form-select" id="car_id" name="car_id">
                @foreach ($cars as $car)
                    <option value="{{ $car->id }}">{{ $car->name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection
