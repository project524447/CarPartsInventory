{{-- resources/views/parts/index.blade.php --}}

@extends('layouts.app')

@section('title', 'Parts List')

@section('content')
    <h1>Parts</h1>
        <a href="{{ route('index') }}" class="btn btn-secondary me-2">Home</a>
        <a href="{{ route('cars.index') }}" class="btn btn-secondary me-2">View Cars</a>
        <a href="{{ route('parts.create') }}" class="btn btn-primary">Add New Part</a>

    <div id="app">  <!-- Vue app -->
        <div class="mt-3">
            <parts-index :initial-parts="{{ json_encode($parts, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE) }}"></parts-index>
        </div>
    </div> <!-- Konec #app -->

    <?php /*
        <div class="mt-3">
            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Serial Number</th>
                        <th>Car</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($parts as $part)
                        <tr>
                            <td>{{ $part->name }}</td>
                            <td>{{ $part->serialnumber }}</td>
                            <td>{{ $part->car->name }}</td>
                            <td>
                                <a href="{{ route('parts.show', $part) }}" class="btn btn-info me-2">View</a>
                                <a href="{{ route('parts.edit', $part) }}" class="btn btn-warning me-2">Edit</a>
                                <form action="{{ route('parts.destroy', $part) }}" method="POST" style="display: inline-block;">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
    </div> */?>

@endsection

@section('scripts')
    <script src="{{ mix('js/app.js') }}"></script>
@endsection        