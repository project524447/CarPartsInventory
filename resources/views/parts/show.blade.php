{{-- resources/views/parts/show.blade.php --}}

@extends('layouts.app')

@section('title', 'View Part')

@section('content')
<div class="container mt-4">
    <h1>View Part</h1>
    <a href="{{ route('parts.index') }}" class="btn btn-secondary mb-3">Back to All Parts</a>
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">{{ $part->name }}</h5>
            <p class="card-text">
                Serial Number: {{ $part->serialnumber }}<br>
                Car: {{ $part->car->name }}
            </p>
        </div>
    </div>
</div>
@endsection

