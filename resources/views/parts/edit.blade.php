{{-- resources/views/parts/edit.blade.php --}}

@extends('layouts.app')

@section('title', 'Edit Part')

@section('content')
<div class="container mt-4">
    <h1>Edit Part</h1>
    <form method="POST" action="{{ route('parts.update', $part) }}">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="name" class="form-label">Part Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ $part->name }}" required>
        </div>
        <div class="mb-3">
            <label for="serialnumber" class="form-label">Serial Number</label>
            <input type="text" class="form-control" id="serialnumber" name="serialnumber" value="{{ $part->serialnumber }}" required>
        </div>
        <div class="mb-3">
            <label for="car_id" class="form-label">Car</label>
            <select class="form-select" id="car_id" name="car_id">
                @foreach ($cars as $car)
                    <option value="{{ $car->id }}" {{ $car->id == $part->car_id ? 'selected' : '' }}>{{ $car->name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection
