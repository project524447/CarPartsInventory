{{-- resources/views/cars/index.blade.php --}}

@extends('layouts.app')

@section('title', 'Cars List')

@section('content')
    <h1>Cars</h1>
        <a href="{{ route('index') }}" class="btn btn-secondary me-2">Home</a>
        <a href="{{ route('parts.index') }}" class="btn btn-secondary me-2">View Parts</a>
        <a href="{{ route('cars.create') }}" class="btn btn-primary">Add New Car</a>
    
    <div id="app">  <!-- Vue app-->
        <div class="mt-3">
            <cars-index :initial-cars="{{ json_encode($cars, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE) }}"></cars-index>
        </div>
    </div> <!-- Koniec #app -->

    <?php /*
    <div class="mt-3">
    <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Registration Number</th>
                        <th>Is Registered</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($cars as $car)
                        <tr>
                            <td>{{ $car->name }}</td>
                            <td>{{ $car->registration_number }}</td>
                            <td>{{ $car->is_registered ? 'Yes' : 'No' }}</td>
                            <td>
                                <a href="{{ route('cars.show', $car) }}" class="btn btn-info">View</a>
                                <a href="{{ route('cars.edit', $car) }}" class="btn btn-warning">Edit</a>
                                <form action="{{ route('cars.destroy', $car) }}" method="POST" style="display: inline-block;">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
    </div> */?>
@endsection

@section('scripts')
    <script src="{{ mix('js/app.js') }}"></script>
@endsection