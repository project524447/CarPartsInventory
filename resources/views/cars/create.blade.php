{{-- resources/views/cars/create.blade.php --}}

@extends('layouts.app')

@section('title', 'Add New Car')

@section('content')
<div class="container mt-4">
    <h1>Add New Car</h1>
    <form method="POST" action="{{ route('cars.store') }}">
        @csrf
        <input type="hidden" name="is_registered" value="0">            
        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" id="name" name="name" required>
        </div>
        <div class="mb-3">
            <label for="registration_number" class="form-label">Registration Number</label>
            <input type="text" class="form-control" id="registration_number" name="registration_number">
        </div>
        <div class="mb-3 form-check">
            <input type="checkbox" class="form-check-input" id="is_registered" name="is_registered" value="1">
            <label class="form-check-label" for="is_registered">Is Registered</label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection

